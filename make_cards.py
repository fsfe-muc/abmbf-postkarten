#!/usr/bin/env python3
import csv
import random
import re
import sys
from itertools               import cycle
from pathlib                 import Path
from pprint                  import pprint
from reportlab.lib.pagesizes import A4, A6
from reportlab.lib.styles    import ParagraphStyle
from reportlab.lib.units     import cm
from reportlab.pdfgen.canvas import Canvas
from reportlab.platypus      import Paragraph, Frame, KeepInFrame

# geometry constants
A4_landscape  = tuple(reversed(A4))
A6_landscape  = tuple(reversed(A6))
width, height = A6_landscape
border        = 0.7*cm
top           = height - border
bottom        = border
left          = border
right         = width  - border
center        = width / 2
padding       = 0.5*cm
vline_x       = center + 1.2*cm
address_x     = vline_x + padding 

# styles
style_greeting = ParagraphStyle(name='greeting',
                                fontSize=8,
                                fontname='DejaVuSans-Bold',
                                spaceAfter=8)
style_body     = ParagraphStyle(name='normal',
                                fontSize=8,
                                spaceAfter=4)


def draw_card(canvas, recipient, text):
    c = canvas

    # fat vertical line, just right of center
    c.setLineWidth(2)
    c.line(vline_x, top, vline_x, bottom)

    # stamp position
    c.setLineWidth(1)
    c.setDash(1, 2)
    c.rect(right - 2.0*cm, top - 2.4*cm, 1.6*cm, 2*cm)
    c.setDash()

    # signature line
    c.setLineWidth(0.5)
    c.setDash(2, 1)
    c.line(left + 2*cm, bottom + 0.5*cm, vline_x - 1*cm, bottom + 0.5*cm)
    c.setDash()

    # draw address
    baseline = height / 2
    for line in map(lambda s: s.strip(), recipient['address']):
        c.drawString(address_x, baseline, line)
        baseline -= 0.7*cm

    # draw text
    text_width  = vline_x - border - padding
    text_height = len(text.split()) * 0.1 * cm  # approx. 0.1cm per word
    pars = re.split(r'\n\n', text)
    paragraphs = ( [Paragraph(recipient['greeting'], style_greeting)] +
                   [Paragraph(par, style_body) for par in pars]
                 )
    in_frame   = KeepInFrame(text_width, text_height, paragraphs)
    frame      = Frame(left,
                       bottom + 0.5*cm,
                       text_width,
                       text_height,
                       showBoundary=0)
    frame.add(in_frame, c)

if __name__ == '__main__':
    # THIS directory
    here = Path(__file__).parent

    # read recipients from recipients.csv
    recipients_path = here / 'recipients.csv'
    with recipients_path.open(encoding='UTF-8') as csv_file:
        reader = csv.DictReader(csv_file,
                                delimiter=',',
                                quotechar='"',
                                fieldnames=('party', 'greeting'),
                                restkey='address')
        recipients = list(reader)

    # read texts from texte/*.txt
    texts_dir_path  = here / 'texte'
    text_paths      = texts_dir_path.glob('*.txt')
    texts           = [(pth.read_text(encoding='UTF-8')) for pth in text_paths ]

    # path of our target file
    target_path = Path(here / 'karten.pdf')

    # canvas for us to draw on
    canvas = Canvas(str(target_path).encode('UTF-8'),
                    pagesize=A4_landscape,
                    pageCompression=True)

    # to distribute four A6 post cards over an A4 page
    # we'll move the canvas origin around like so:
    movements = cycle([(  width,      0),
                       (      0, height),
                       (-width,       0),
                       (     0, -height)])

    # loop for great success
    for num, (recipient, movement) in enumerate(zip(recipients, movements),
                                                start=1):

        # on every new A4 page: draw cutting marks
        if num % 4 == 1:
            canvas.lines([(width - 0.2*cm, height, width + 0.2*cm, height),
                          (width, height - 0.2*cm, width, height + 0.2*cm)])

        # choose a random text
        text = random.choice(texts)

        # print progress to stdout
        print(str.format('printing card {:02d} ({}...)',
                         num, recipient['greeting']))

        # draw card onto canvas
        draw_card(canvas, recipient, text)

        # move canvas origin
        canvas.translate(*movement)

        # every 4th card: save page
        if num % 4 == 0:
            canvas.showPage()

    canvas.save()
